import { ChangeDetectorRef, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OntoIndividualDto } from 'semn-lib';
import { StatefulComponent } from 'src/app/common-ui/stateful-component';
import { AppStore } from 'src/app/services/app-store';

@Component({
  selector: 'list-entities',
  templateUrl: './list-entities.component.html',
  styleUrls: ['./list-entities.component.scss'],
})
export class ListEntitiesComponent extends StatefulComponent {
    constructor(
      cdRef: ChangeDetectorRef,
      public appStore: AppStore,
      private route: ActivatedRoute,
      private router: Router
    ) {
      super(cdRef);
    }

    showDetailsFor(note: OntoIndividualDto) {
      this.router.navigateByUrl(`/tabs/main/details/${note.getLocalId()}`);
    }
  }
