import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { first, isEmpty } from 'lodash';
import { compareOntoPath, DataIRIs, getClassPath, OntoClassDto, OntoIndividualDto } from 'semn-lib';
import { StatefulComponent } from 'src/app/common-ui/stateful-component';
import { AppStore } from 'src/app/services/app-store';
import { OntoEntityUtils } from 'src/app/utils/onto-entity.utils';
import { none, some } from 'ts-option';
import { isArray } from 'util';

@Component({
  selector: 'list-entity-detail',
  templateUrl: './list-entity-detail.page.html',
  styleUrls: ['./list-entity-detail.page.scss'],
})
export class ListEntityDetailPage extends StatefulComponent implements OnInit {

  entity: OntoIndividualDto;
  classes: OntoClassDto[][] = [];

  constructor(
    cdRef: ChangeDetectorRef,
    private route: ActivatedRoute,
    public appStore: AppStore,
    public ontoUtils: OntoEntityUtils
  ) {
    super(cdRef);
    this.addSubs([
      this.route.params.subscribe((data: { id: string }) => {
        this.appStore
          .queryNotes((el) => el.getLocalId() === data.id)
          .flatMap(el => isEmpty(el) ? none : some(first(el)))
          .forEach(el => {
            this.entity = el;
            const rootClass = first(this.appStore.selectedThread.classes
              .map(el => isArray(el) ? el : [el])
              .getOrElse([])
            );
            this.classes = this.entity.classes
              .reduce((res, value) => {
                res.push(getClassPath(rootClass, value.value));
                return res;
              }, [])
              .sort(compareOntoPath);
          });
      }),
    ]);
  }

  ngOnInit() {}

}
