import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { isEmpty } from 'lodash';
import { filter } from 'rxjs/operators';
import { StatefulComponent } from 'src/app/common-ui/stateful-component';
import { AuthService } from 'src/app/utils/auth.service';
import { option } from 'ts-option';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage extends StatefulComponent implements OnInit {
  constructor(
    cdRef: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    private auth: AuthService
  ) {
    super(cdRef);
    this.addSubs([
      this.route.queryParams.subscribe((params: { access: string }) => {
        option(params.access)
          .filter(data => !isEmpty(data))
          .forEach((token) => {
            this.auth.loginByToken(params.access);
          });
      }),
    ]);
  }

  ngOnInit() {
  }

}
