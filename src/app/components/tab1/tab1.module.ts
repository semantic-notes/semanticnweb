import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { CommonUiModule } from 'src/app/common-ui/common-ui.module';
import { ServicesModule } from 'src/app/services/services.module';

import { Tab1Page } from './tab1.page';
import { ListEntitiesComponent } from '../pages/list-entities/list-entities.component';
import { ListEntityDetailPage } from '../pages/list-entity-detail/list-entity-detail.page';

const routes: Routes = [
  {
    path: '',
    component: Tab1Page,
    children: [
      {
        path: 'all',
        component: ListEntitiesComponent
      },
      {
        path: 'details/:id',
        component: ListEntityDetailPage
      },
    ],
  },
  {
    path: '',
    redirectTo: 'all',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes),
    ServicesModule,
    CommonUiModule,
  ],
  declarations: [Tab1Page, ListEntitiesComponent, ListEntityDetailPage]
})
export class Tab1PageModule {}
