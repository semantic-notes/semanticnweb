import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IonRouterOutlet } from '@ionic/angular';
import { OntoIndividualDto } from 'semn-lib';
import { StatefulComponent } from 'src/app/common-ui/stateful-component';
import { AppStore } from 'src/app/services/app-store';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class Tab1Page extends StatefulComponent implements OnInit {
  @ViewChild(IonRouterOutlet) childOutlet: IonRouterOutlet;
  constructor(
    cdRef: ChangeDetectorRef,
    public appStore: AppStore,
    private route: ActivatedRoute,
    private router: Router,
  ) {
    super(cdRef);
  }

  ngOnInit(): void {}

  showDetailsFor(note: OntoIndividualDto) {
    console.log(this.childOutlet);
    this.router.navigateByUrl(`/tabs/main/details/${note.getLocalId()}`);
  }
}
