import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes, Router } from '@angular/router';
import { IonicStorageModule } from '@ionic/storage';

import { DefaultRequestOptions } from './utils/default-request-options.service';
import { AuthorizedActivate } from './utils/route-guard';
import { UtilsModule } from './utils/utils.module';
import { AuthService } from './utils/auth.service';
import { filter } from 'rxjs/operators';
import { ServicesModule } from './services/services.module';

const routes: Routes = [
  {
    path: '',
    loadChildren: './components/tabs/tabs.module#TabsPageModule',
    canActivate: [ AuthorizedActivate ]
  },
  { path: 'login', loadChildren: './components/login/login.module#LoginPageModule' },
];
@NgModule({
  imports: [
    UtilsModule.forRoot(),
    ServicesModule.forRoot(),
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
    IonicStorageModule.forRoot({
        name: '__semndb',
    })
  ],
  exports: [RouterModule, UtilsModule],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: DefaultRequestOptions,
      multi: true,
    },
    { provide: 'BackendUrl', useValue: '/api' },
  ]
})
export class AppRoutingModule {
  constructor(
    private auth: AuthService,
    private router: Router,
  ) {
    this.auth.userData
      .pipe(filter(el => el.isDefined))
      .subscribe(data => {
        data.match({
          some: (_) => this.router.navigateByUrl(''),
          none: () => this.router.navigateByUrl('/login')
        });
      });
  }
}
