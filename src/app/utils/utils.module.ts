import { ModuleWithProviders, NgModule } from '@angular/core';

import { AuthService } from './auth.service';
import { OntoEntityUtils } from './onto-entity.utils';
import { AuthorizedActivate, AuthorizedActivateChild, RouteGuard } from './route-guard';
import { TokenService } from './token.service';

@NgModule({})
export class UtilsModule {
  static forRoot(): ModuleWithProviders<UtilsModule> {
    return {
      ngModule: UtilsModule,
      providers: [
        TokenService,
        AuthService,
        RouteGuard,
        AuthorizedActivate,
        AuthorizedActivateChild,
        OntoEntityUtils,
      ],
    };
  }
}
