import * as _ from 'lodash';
import {Injectable} from '@angular/core';
import {
  HttpHandler,
  HttpHeaderResponse,
  HttpInterceptor,
  HttpProgressEvent,
  HttpRequest,
  HttpResponse,
  HttpSentEvent,
  HttpUserEvent
} from '@angular/common/http';
import { TokenService } from './token.service';
import { Observable } from 'rxjs';


@Injectable()
export class DefaultRequestOptions implements HttpInterceptor {
  static authHeaderName = 'authorization';
  private token: string = '';

  constructor(tokenService: TokenService) {
    const token = tokenService.data ? tokenService.data.token : null;
    if (token) {
      this.setAuthTokenHeader(token);
    }
    tokenService.dataChanged.subscribe((data) => {
      if (data) {
        this.setAuthTokenHeader(data.token);
      } else {
        this.setAuthTokenHeader('');
      }
    });
  }

  setAuthTokenHeader(token) {
    this.token = token;
  }

  intercept(
    req: HttpRequest<any>, next: HttpHandler)
      : Observable<HttpSentEvent | HttpHeaderResponse
        | HttpProgressEvent | HttpResponse<any>
        | HttpUserEvent<any>> {
    if (!_.isEmpty(this.token)) {
      const modified = req.clone({
        headers: req.headers.set(DefaultRequestOptions.authHeaderName, this.token)
      });
      return next.handle(modified);
    } else {
      return next.handle(req);
    }
  }
}
