import { Inject, Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { BehaviorSubject, Observable } from 'rxjs';
import { OntoService, User, UserAuth } from 'semn-lib';
import { none, Option, option, some } from 'ts-option';

@Injectable()
export class AuthService {
  ontoService: OntoService;
  private _authData: BehaviorSubject<Option<UserAuth>> = new BehaviorSubject(none);
  private _userData: BehaviorSubject<Option<User>> = new BehaviorSubject(none);

  get authData(): Observable<Option<UserAuth>> {
    return this._authData;
  }

  get userData(): Observable<Option<User>> {
    return this._userData;
  }

  get isAuthorized(): boolean {
    return this._authData.value.isDefined;
  }

  get authInfo(): Option<UserAuth> {
    return this._authData.value;
  }

  constructor(
    @Inject('BackendUrl') backend: string,
    public storage: Storage
  ) {
    this.ontoService = new OntoService(backend);
    this.storage.get('auth').then(data => {
      option(data).forEach((data) => {
        const auth = some(this.ontoService.deserialize(data, UserAuth));
        this._authData.next(auth);
      });
    });
    this.storage.get('user').then((data) => {
      option(data).forEach((data) => {
        const user = some(this.ontoService.deserialize(data, User));
        this._userData.next(user);
      });
    });
  }

  handleAuthData(data: Option<UserAuth>): Promise<Option<User>> {
    this._authData.next(data);
    return data.match({
      some: (auth) => {
        this.storage.set('auth', auth);
        return this.ontoService.getUserData(auth);
      },
      none: () => Promise.resolve(none)
    }).then(user => {
      this.storage.set('user', user.orNull);
      this._userData.next(user);
      return user;
    });
  }

  loginByToken(token: string): Promise<Option<User>> {
    return this.ontoService.loginUserByToken(token)
      .then(data => this.handleAuthData(data));
  }

  refreshToken() {
    this.authInfo.forEach((auth) => {
      this.ontoService.refreshToken(auth)
        .then(data => this.handleAuthData(data));
    });
  }

}
