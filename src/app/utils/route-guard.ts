import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable()
export class RouteGuard {
  constructor(
    public router: Router,
    private auth: AuthService
  ) {}

  isLoggedIn(): boolean {
    return this.auth.isAuthorized;
  }

  canActivate() {
    if (this.isLoggedIn()) {
      return true;
    } else {
      const path = window.location.pathname;
      this.router.navigate([`login`]);
      return false;
    }
  }
}

@Injectable()
export class AuthorizedActivateChild implements CanActivateChild {
  constructor(private guard: RouteGuard) {}

  canActivateChild(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean>|Promise<boolean>|boolean {
    return this.guard.canActivate();
  }
}

@Injectable()
export class AuthorizedActivate implements CanActivate {
  constructor(private guard: RouteGuard) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean>|Promise<boolean>|boolean {
    return this.guard.canActivate();
  }
}
