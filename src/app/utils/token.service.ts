import { EventEmitter, Injectable } from '@angular/core';
import { UserAuth } from 'semn-lib';

@Injectable()
export class TokenService {
  data: UserAuth;

  dataChanged: EventEmitter<UserAuth> = new EventEmitter();

  constructor() {}

  setToken(data: UserAuth) {
    this.data = data;
    this.dataChanged.emit(this.data);
  }

  clearToken() {
    this.data = null;
    this.dataChanged.emit(this.data);
  }
}
