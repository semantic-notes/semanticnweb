import { Injectable } from '@angular/core';
import { DataIRIs, OntoIndividualDto } from 'semn-lib';
import { option } from 'ts-option';

@Injectable()
export class OntoEntityUtils {
  static getText(note: OntoIndividualDto): string[] {
    return note.getData(DataIRIs.hasText);
  }

  static getTitle(note: OntoIndividualDto): string {
    return option(note.getData(DataIRIs.hasTitle)[0]).getOrElse('');
  }

  getText(note: OntoIndividualDto): string[] { return OntoEntityUtils.getText(note); }
  getTitle(note: OntoIndividualDto): string { return OntoEntityUtils.getTitle(note); }
}
