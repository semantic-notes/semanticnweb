import { autorun } from 'mobx';

export function startObserving(component: any, expression: () => void) {
  const disposer = autorun(expression);
  if (!component.__mobx_unsubs) {
    component.__mobx_unsubs = [disposer];
  } else {
    component.__mobx_unsubs.push(disposer);
  }
}

export function stopObserving(component: any) {
  if (!component.__mobx_unsubs) {
    throw new Error(
      "Stopped observing without any observers. Did you screw up?"
    );
  }
  component.__mobx_unsubs.forEach(disposer => disposer());
}
