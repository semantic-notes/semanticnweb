import { ModuleWithProviders, NgModule } from '@angular/core';
import { MobxAngularModule } from 'mobx-angular';

import { AppStore } from './app-store';
import { OntoService } from './onto.service';

@NgModule({
  imports: [ MobxAngularModule ],
  exports: [ MobxAngularModule ]
})
export class ServicesModule {
  static forRoot(): ModuleWithProviders<ServicesModule> {
    return {
      ngModule: ServicesModule,
      providers: [
        AppStore,
        OntoService
      ],
    };
  }
}
