import { Inject, Injectable } from '@angular/core';
import {
  CreateNoteDto,
  ElementType,
  ISerializerSettings,
  OntoIndividualDto,
  OntoService as SemNOnto,
  IOnto,
  User,
  UserAuth,
  UserLoginToken,
  UserThread,
  CreateThread,
} from 'semn-lib';
import { Option } from 'ts-option';
import * as HttpStatus from 'http-status-codes';
import { AuthService } from '../utils/auth.service';

@Injectable()
export class OntoService implements IOnto {
  private semNOnto: SemNOnto;

  constructor(
    @Inject('BackendUrl') backend: string,
    private auth: AuthService,
  ) {
    this.semNOnto = new SemNOnto(backend);
    this.semNOnto.errors.subscribe(error => {
      if (error.status === HttpStatus.FORBIDDEN) {
        this.auth.refreshToken();
      }
    });
  }

  deserialize<T>(json: Object, type: ElementType<T>, settings?: ISerializerSettings): T {
    return this.semNOnto.deserialize(json, type, settings);
  }

  serialize<T>(obj: T, settings?: ISerializerSettings): T {
    return this.semNOnto.serialize(obj, settings);
  }

  loginUserByToken(token: string): Promise<Option<UserAuth>> {
    return this.semNOnto.loginUserByToken(token);
  }

  loginUserFromTelegram(user: User): Promise<Option<UserAuth>> {
    throw new Error("Method not implemented.");
  }

  refreshToken(auth: UserAuth): Promise<Option<UserAuth>> {
    return this.semNOnto.refreshToken(auth);
  }

  genLoginToken(user: User, auth: UserAuth): Promise<Option<UserLoginToken>> {
    throw new Error("Method not implemented.");
  }

  getUserData(auth: UserAuth): Promise<Option<User>> {
    return this.semNOnto.getUserData(auth);
  }

  getThread(auth: UserAuth, uuid: string): Promise<Option<UserThread>> {
    return this.semNOnto.getThread(auth, uuid);
  }

  getThreads(auth: UserAuth): Promise<Option<UserThread[]>> {
    return this.semNOnto.getThreads(auth);
  }

  addNoteToThread(auth: UserAuth, thread: UserThread, note: CreateNoteDto): Promise<any> {
    return this.semNOnto.addNoteToThread(auth, thread, note);
  }

  getNotesInThread(auth: UserAuth, thread: UserThread): Promise<Option<OntoIndividualDto[]>> {
    return this.semNOnto.getNotesInThread(auth, thread);
  }

  createThread(auth: UserAuth, thread: CreateThread): Promise<void> {
     return this.semNOnto.createThread(auth, thread);
  }
}
