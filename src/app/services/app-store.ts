import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { action, observable, computed } from 'mobx-angular';
import { filter } from 'rxjs/operators';
import { User, UserThread, OntoIndividualDto, SystemClass } from 'semn-lib';
import { Option, some, option } from 'ts-option';

import { AuthService } from '../utils/auth.service';
import { OntoService } from './onto.service';
import { chain } from 'lodash';

@Injectable()
export class AppStore {
  @observable user: User;
  @observable userThreads: UserThread[] = [];
  @observable selectedThread: UserThread;
  @observable notes: OntoIndividualDto[] = [];

  constructor(
    private storage: Storage,
    private auth: AuthService,
    private ontoService: OntoService
  ) {
    this.auth.userData
      .pipe(filter(el => el.isDefined))
      .subscribe(data => {
        this.setUser(data);
      });
  }

  @action setUser(user: Option<User>) {
    user.forEach((user) => {
      this.user = user;
      this.auth.authInfo.forEach(auth => {
        this.ontoService.getThreads(auth).then(res => {
          this.setThreads(res.getOrElse([]));
        });
      });
    });
  }

  @action setThreads(items: UserThread[]) {
    this.userThreads = items;
    option(this.selectedThread).match({
      none: () => {
        this.storage.get('selectedThread').then((data) => {
          option(data).forEach(data => {
            const value = this.ontoService.deserialize(data, UserThread);
            this.selectThread(value);
          });
        });
      },
      some: () => {}
    });
  }

  @action selectThread(thread: UserThread) {
    if (chain(this.userThreads).some(el => el.uuid === thread.uuid).value()) {
      this.auth.authInfo.forEach(auth => {
        this.ontoService.getThread(auth, thread.uuid).then((data: Option<UserThread>) => {
          data.forEach((thread) => {
            this.selectedThread = thread;
            this.selectedThread.individuals
              .map(items => items.filter(el => el.hasClassIri(SystemClass.Note)))
              .forEach((notes) => {
                this.setNotes(notes);
              });
            this.storage.set('selectedThread', this.ontoService.serialize(this.selectedThread));
          });
        });
      });
    }
  }

  isSelected(thread: UserThread): boolean {
    return option(thread).map(el => el.uuid === thread.uuid).getOrElse(false);
  }

  @action setNotes(items: OntoIndividualDto[]) {
    this.notes = items;
  }

  @action loadNotesInThread(event?) {
    this.auth.authInfo.map((auth) => {
      return option(this.selectedThread).match({
        some: (thread) => ({ auth: auth, thread: thread }),
        none: () => null
      });
    }).forEach(data => {
      this.ontoService.getNotesInThread(data.auth, data.thread)
        .then((result) => {
          option(event).forEach((evt) => {
            evt.target.complete();
          });
          result.forEach((notes) => {
            this.setNotes(notes);
          });
        });
    });
  }

  queryNotes(query: (el: OntoIndividualDto) => boolean): Option<OntoIndividualDto[]> {
    return option(this.notes.filter(query));
  }

}
