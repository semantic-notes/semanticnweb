import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SemnHeaderComponent } from './semn-header/semn-header.component';
import { ServicesModule } from '../services/services.module';
import { UtilsModule } from '../utils/utils.module';
import { SemnNoteComponent } from './semn-note/semn-note.component';
import { RouterModule } from '@angular/router';
import { NotesFilterPipe } from './pipes/notes-filter.pipe';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    UtilsModule,
    ServicesModule,
    RouterModule,
  ],
  declarations: [SemnHeaderComponent, SemnNoteComponent, NotesFilterPipe],
  exports: [SemnHeaderComponent, SemnNoteComponent, NotesFilterPipe]
})
export class CommonUiModule {}
