import { Pipe, PipeTransform } from '@angular/core';
import { OntoIndividualDto, SystemClass } from 'semn-lib';
import { isEmpty } from 'lodash';

@Pipe({
  name: 'notesFilter'
})
export class NotesFilterPipe implements PipeTransform {

  transform(values: OntoIndividualDto[], args?: any): OntoIndividualDto[] {
   return values
    .filter((el) => el.classes.some((el) => el.value === SystemClass.Unhandled));
  }

}
