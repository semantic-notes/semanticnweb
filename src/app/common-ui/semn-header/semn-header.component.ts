import { Component, OnInit, Optional, Input } from '@angular/core';
import { AppStore } from 'src/app/services/app-store';
import { MenuController, IonRouterOutlet } from '@ionic/angular';

@Component({
  selector: 'semn-header',
  templateUrl: './semn-header.component.html',
  styleUrls: ['./semn-header.component.scss'],
})
export class SemnHeaderComponent implements OnInit {

  @Input() ionRouterOutlet: IonRouterOutlet;

  constructor(
    public appStore: AppStore,
    private menu: MenuController,
    @Optional() private routerOutlet: IonRouterOutlet,
  ) { }

  ngOnInit() {}

  toggleMenu() {
    this.menu.toggle();
  }

  canGoBack(): boolean {
    return this.ionRouterOutlet && this.ionRouterOutlet.canGoBack();
  }

}
