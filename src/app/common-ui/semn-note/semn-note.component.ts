import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DataIRIs, getShortName, OntoIndividualDto, SystemClass } from 'semn-lib';

import { StatefulComponent } from '../stateful-component';
import { option } from 'ts-option';
import { OntoEntityUtils } from 'src/app/utils/onto-entity.utils';

@Component({
  selector: 'semn-note',
  templateUrl: './semn-note.component.html',
  styleUrls: ['./semn-note.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SemnNoteComponent extends StatefulComponent implements OnInit {

  @Input() note: OntoIndividualDto;
  @Output() detailsShown: EventEmitter<OntoIndividualDto> = new EventEmitter();

  constructor(
    cdRef: ChangeDetectorRef,
    public ontoUtils: OntoEntityUtils
  ) { super(cdRef); }

  ngOnInit() {}

  getClasses(note: OntoIndividualDto): string[] {
    return note.getNameClasses();
  }

  isUnhandled(cls: string): boolean {
    return getShortName(SystemClass.Unhandled) === cls;
  }

  showDetails() {
    this.detailsShown.next(this.note);
  }

}
