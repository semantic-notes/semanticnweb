import { ChangeDetectorRef, EventEmitter, OnDestroy, Output } from '@angular/core';
import { isFunction, isUndefined } from 'lodash';
import { Subscription } from 'rxjs';

export class StatefulComponent implements OnDestroy {

  @Output() stateChanged: EventEmitter<void> = new EventEmitter();

  get wasDestroyed(): boolean {
    return this._wasDestroyed;
  }

  protected subs: Subscription[] = [];
  private _wasDestroyed: boolean = false;

  constructor(protected cdRef: ChangeDetectorRef) {}

  ngOnDestroy(): void {
    this._wasDestroyed = true;
    this.stateChanged.unsubscribe();
    this.subs.forEach(el => el.unsubscribe());
  }

  protected detectChanges() {
    if (!this.wasDestroyed) {
      this.cdRef.markForCheck();
      this.cdRef.detectChanges();
    }
  }

  protected addSubs(subs: Subscription[]) {
    this.subs.push(...subs);
  }

  protected clearAllSubs() {
    this.subs.forEach(el => el.unsubscribe());
    this.subs = [];
  }

  protected setState<T = void>(func: () => T = () => void(0)): T {
    if (isFunction(func)) {
      const ret = func ? func() : undefined;
      this.detectChanges();
      if (!this.stateChanged.isStopped) {
        this.stateChanged.emit();
      }
      if (!isUndefined(ret)) {
        return ret;
      }
    }
  }

}
